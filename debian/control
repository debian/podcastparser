Source: podcastparser
Section: python
Priority: optional
Maintainer: tony mancill <tmancill@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               dh-python,
               dh-sequence-python3,
               python3-all,
               python3-coverage,
               python3-pytest,
               python3-pytest-cov,
               python3-setuptools,
               python3-sphinx
Standards-Version: 4.6.2
Homepage: https://github.com/gpodder/podcastparser
Vcs-Browser: https://salsa.debian.org/debian/podcastparser
Vcs-Git: https://salsa.debian.org/debian/podcastparser.git
Rules-Requires-Root: no

Package: python3-podcastparser
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-podcastparser-doc
Description: Simplified, fast RSS parsing library (Python 3)
 The podcast parser project is a library from the gPodder project to
 provide an easy and reliable way of parsing RSS- and Atom-based podcast
 feeds in Python.
 .
 This package installs the library for Python 3.

Package: python-podcastparser-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: Simplified, fast RSS parsing library (common documentation)
 The podcast parser project is a library from the gPodder project to
 provide an easy and reliable way of parsing RSS- and Atom-based podcast
 feeds in Python.
 .
 This is the common documentation package.
